from mixer.backend.flask import mixer
from pytest import fixture
from flaskr import create_app


@fixture
def app():
    """Define the app as a fixture whose context is required for essentially every
    function of this application."""

    app = create_app(test_config="config.TestingConfig")
    mixer.init_app(app)

    yield app


@fixture
def db(app):
    """Define the database to be reset before and after every function. This ensures that
    no data exists in the database between runs and also that the database is fresh for
    each run."""

    from flaskr.models import db

    with app.app_context():
        db.drop_all()
        db.create_all()

    yield db

    with app.app_context():
        db.drop_all()
        db.create_all()


@fixture
def password():
    """Fixture for generating a password that can be used to create User models. The
    password must be specially crafted using bcrypt or it won't save correctly. The
    password used here is always "password"."""

    from bcrypt import hashpw, gensalt

    password = hashpw("password".encode(), gensalt()).decode()

    yield password
